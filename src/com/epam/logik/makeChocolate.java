package com.epam.logik;//We want make a package of goal kilos of chocolate. We have small bars (1 kilo each) and big bars (5 kilos each). Return the number of small bars to use, assuming we always use big bars before small bars. Return -1 if it can't be done.
//
//
//        com.epam.logik.makeChocolate(4, 1, 9) → 4
//        com.epam.logik.makeChocolate(4, 1, 10) → -1
//        com.epam.logik.makeChocolate(4, 1, 7) → 2

public class makeChocolate {
    public int makeChocolate(int small, int big, int goal) {
        int usedBig;
        usedBig = goal/5;
        if (small + usedBig*5 >= goal && usedBig <= big)
            return goal-usedBig*5;
        else if(usedBig > big && big*5 + small >=goal)
            return goal-big*5;
        else
            return -1;
    }
}
