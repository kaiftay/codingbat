//Return the number of times that the string "code" appears anywhere in the given string, except we'll accept any letter for the 'd', so "cope" and "cooe" count.
//
//
//        countCode("aaacodebbb") → 1
//        countCode("codexxcode") → 2
//        countCode("cozexxcope") → 2
package com.epam.array;

public class CountCode {
    public int countCode(String str) {
        int startIndex = str.indexOf("co");
        int count = 0;
        while (startIndex != -1 && startIndex + 3 < str.length()) {
            if (str.charAt(startIndex+3) == 'e' && isEnglishLetter(str.charAt(startIndex + 2))) {
                count++;
            }
            startIndex = str.indexOf("co", startIndex+1);
        }
        return count;
    }

    public boolean isEnglishLetter(char charAt) {
        return charAt >= 65 || charAt <= 122;
    }
}
