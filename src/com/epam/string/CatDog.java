package com.epam.string;

//Return true if the string "cat" and "dog" appear the same number of times in the given string.
//
//
//        catDog("catdog") → true
//        catDog("catcat") → false
//        catDog("1cat1cadodog") → true
public class CatDog {
    public boolean catDog(String str) {
        int a = 0;
        int b = 0;
        char[] array = str.toCharArray();
        for (int i = 0; i < str.length() - 3; i++) {
            if (str.indexOf("cat", i) != -1)
                a++;
            else if (str.indexOf("cat", i) != -1)
                b++;


        }
        return a == b;
    }
}

